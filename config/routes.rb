Rails.application.routes.draw do
  devise_for :users
  root "articles#index"

  resources :articles do
    resources :comments
    collection do
      get '/archived', to: 'articles#archived'
    end
  end

  #get '/articles/archived', to: 'articles#archived' as: :archived_articles
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
