class ArticlePolicy < ApplicationPolicy   
  def index?
    true
  end

  def show?
    if record.status == 'public'
      true
    else
      author? || admin?
    end
  end

  def archived? 
    user_logged_in?
  end

  def update?
    author? || admin?
  end

  def destroy?
    author? || admin?
  end

  private

  def author?
    user_logged_in? && record.author_id == user.id
  end
end