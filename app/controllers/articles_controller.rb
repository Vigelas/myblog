class ArticlesController < ApplicationController

  #http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]
  before_action :authenticate_user!, except: [:index, :show]
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  #before_action :autorize_user!, only: [:edit, :destroy, :update]

  def index
    authorize Article
    @articles = Article.published.ordered.with_authors
  end

  def archived
    authorize Article
    @articles = current_user.admin? ? Article.all : current_user.articles
    @articles = @articles.non_published.ordered.with_authors
  end

  def show
    authorize @article
  end

  def new
    authorize Article
    @article = Article.new
  end

  def create 
    authorize Article
    @article = Article.new(article_params)
    
    @article.author = current_user

    if @article.save
      redirect_to @article 
    else 
      render :new
    end
  end

  def edit
    authorize @article
  end

  def update
    authorize @article
    if @article.update(article_params)
      redirect_to @article
    else
      render :edit
    end
  end

  def destroy
    authorize @article
    @article.destroy

    redirect_to root_path
  end

  private
    def article_params
      params.require(:article).permit(:title, :body, :status);
    end

    def set_article
      @article = Article.find(params[:id])
    end

    #def autorize_user!
    #  return if @article.author_id == current_user.id  
    #  redirect_to :articles, alert: "You don`t have permission to perform this action."
    #end
end
