class CommentsController < ApplicationController

  #http_basic_authenticate_with name: "dhh", password: "secret", only: :destroy
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :set_article, only: [:create, :destroy]
  before_action :autorize_user!, only: :destroy

  def create
      @comment = @article.comments.create(comment_params)
      @comment.commenter = @article.author.email
      redirect_to article_path(@article)
  end

  def destroy
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to article_path(@article)
  end
  
  private
    def comment_params
      params.require(:comment).permit(:body, :status)
    end

    def set_article
      @article = Article.find(params[:article_id])
    end

    def autorize_user!
      return if @article.author_id == current_user.id  

      redirect_to :articles, alert: "You don`t have permission to perform this action."
    end
end
