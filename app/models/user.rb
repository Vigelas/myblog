class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable, , :rememberable
  devise :database_authenticatable, :registerable, :recoverable, :validatable

  has_many :articles, dependent: :destroy, foreign_key: :author_id

  ROLES = {
    user: 0,
    admin: 1
  }.with_indifferent_access.freeze

  enum role: ROLES
end
